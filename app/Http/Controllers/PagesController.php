<?php
  namespace App\Http\Controllers;

  class PagesController extends Controller{
    public function getMainPage(){
      return view('pages/main', ['var1'=>'Ephraim Lambarte']);
    }
    public function getAdminPost(){
      return view('adminView/posts/adminPost');
    }
  }
?>

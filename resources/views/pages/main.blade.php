@extends('mainTemplate')
@section('title', '| Home')
@section('mycss')
  <link rel = 'stylesheet' href = '{{asset('css/home.css')}}'>
@endsection

@section('content')
  <div class='row'>
    <div id="custom-bootstrap-carousel" class="carousel slide" data-ride="carousel" data-interval="5000">
        <ol class="carousel-indicators">
            <li data-target="#custom-bootstrap-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#custom-bootstrap-carousel" data-slide-to="1"></li>
            <li data-target="#custom-bootstrap-carousel" data-slide-to="2"></li>
            <li data-target="#custom-bootstrap-carousel" data-slide-to="3"></li>
            <li data-target="#custom-bootstrap-carousel" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="{{asset('images/1.jpg')}}" alt="Lisbon" class='carouselImage'>
                <div class="carousel-caption">The is a new first slide</div>
            </div>
            <div class="item">
                <img src="{{asset('images/2.jpg')}}" class='carouselImage'>
                <div class="carousel-caption">The is the second slide</div>
            </div>
            <div class="item">
                <img src="{{asset('images/3.JPEG')}}" class='carouselImage'>
            </div>
            <div class="item">

                    <img src="{{asset('images/4.jpg')}}" class='carouselImage'>
                    <div class="carousel-caption">Fourth slide</div>

            </div>
            <div class="item">

                    <img src="{{asset('images/5.jpg')}}" class='carouselImage'>

            </div>
        </div><a class="left carousel-control" href="#custom-bootstrap-carousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="right carousel-control"
        href="#custom-bootstrap-carousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span></a>
    </div>
  </div>
  <div style = 'margin-top:20px;'>
    <div class='row'>
      <div class='col-md-4 myCard'>
        <div>
          <img class='cardImage' src="{{asset('images/5.jpg')}}">
        </div>
        <div>
          <h4>Pipe Crushed Nugget</h4>
          <p>
            Very delicious weed for your morning breakfast
          </p>
        </div>
      </div>
      <div class='col-md-4'>
      </div>
    </div>
  </div>

@endsection

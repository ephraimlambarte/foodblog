<div class='row'>
<div id="custom-bootstrap-menu" class="navbar navbar-default " role="navigation">
    <div class="container-fluid">
        <div class="navbar-header"><a class="navbar-brand" href="#">Herb Kitchen</a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-menubuilder">
            <ul class="nav navbar-nav navbar-left">
                <li id = 'home'><a href="/main">Home</a>
                </li>
                <li><a href="#">Recipes</a>
                </li>
                <li><a href="#">About Us</a>
                </li>
                <li><a href="#">Contact Us</a>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>

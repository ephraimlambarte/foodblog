@if(Session::has('message'))
  <div class='alert alert-success' role = 'alert' style = 'margin-top:20px;'>
    <strong>Success:</strong>{{Session::get('message')}}
  </div>
@endif

@if(count($errors)>0)
  <div class='alert alert-danger' role = 'alert' style = 'margin-top:20px;'>
    <strong>Error:</strong>Something went wrong.
  </div>
@endif

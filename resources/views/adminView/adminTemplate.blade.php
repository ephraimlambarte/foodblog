<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials._headerCss')
    <title>Admin Panel @yield('title')</title>
    @yield('mycss')
  </head>
  <body>
    <div class='container'>
    @include('partials._adminNavBar')
    @yield('content')
    @include('partials.successMessages._genericMessage')
    @include('partials._scripts')
    <div class='row' id = 'footer'>
      <b>Admin Panel</b> - Pierre
    </div>
    </div>
    @yield('script')
  </body>
</html>

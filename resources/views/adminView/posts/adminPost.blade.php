@extends('adminView.adminTemplate')
@section('title', '| insertRecipe')
@section('mycss')
  <link rel = 'stylesheet' href = '{{asset('css/admin.css')}}'>
@endsection
@section('content')

  <div class='row' id = 'rowcontainer'>
    {!! Form::open(['route' => 'posts.store'], array('data-parsley-validate'=>
                                                    '')) !!}
      {{Form::label('recipeName', 'Title:')}}
      {{Form::text('recipeName', null, array('class' =>'form-control',
                                            'required' =>'',
                                             'maxlength'=>'200'))}}

      {{Form::label('description', 'Description:')}}
      {{Form::textarea('description', null, array('class' =>'form-control',
                                                  'rows'=>'3',
                                                  'required'=>''))}}

      {{Form::label('process', 'Procedure:')}}
      {{Form::textarea('process', null, array('class' =>'form-control',
                                              'rows'=>'6',
                                              'required'=>''))}}
      {{Form::submit('Insert Recipe', array('class'=>'btn btn-default btn-block',
                                            'style'=>'margin-top:20px;'))}}
    {!! Form::close() !!}

  </div>
@endsection
@section('script')
  <script src="{{asset('js/admin/postValidations.js')}}"></script>
@endsection

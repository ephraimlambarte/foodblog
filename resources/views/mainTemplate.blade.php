<!DOCTYPE html>
<html lang="en">
  <head>
    @include('partials._headerCss')
    <title>Herb Kitchen @yield('title')</title>
    @yield('mycss')
  </head>
  <body>
    <div class='container'>
    @include('partials._navBar')
    @yield('content')
    @include('partials._scripts')
    <div class='row' id = 'footer'>
      <b>Herb Kitchen</b> - Pierre
    </div>
    </div>

  </body>
</html>
